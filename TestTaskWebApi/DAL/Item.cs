
using System.ComponentModel.DataAnnotations;



namespace TestTaskWebApi.DAL
{
   
    public class Item
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]     
        public string Type { get; set; }
    }
}
