﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTaskWebApi.Models
{
    public class StatisticDTO
    {
        public string Type { get; set; }
        public int Count { get; set; }
    }
}